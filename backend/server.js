'use strict';
const express = require('express');
const { createElement, deleteElement, getAll, updateElement } = require('./mongo/mongo')
const PORT = 3001;

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/elements', (req, res) => {
    createElement(req.body).catch(console.log);
    res.status(201).end();
});

app.delete('/elements', (req, res) => {
    deleteElement(req.body).catch(console.log);
    res.status(204).end();
});

app.put('/elements', (req, res) => {
    updateElement(req.body).catch(console.log);
    res.status(204).end();
});

app.get("/elements", async (req, res) => {
    const elements = await getAll().catch(console.log);

    console.log(elements);
    res.status(200).json({ list: elements }).end();
});

app.listen(PORT, () => {
    console.log(`Running on http://localhost:${PORT}`);
});
