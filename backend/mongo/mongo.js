const mongodb = require('mongodb');

const uri = "mongodb://mongo:27017/mongo-test";
const client = new mongodb.MongoClient(uri);

async function createElement(el) {
    try {
        await client.connect();
        const database = client.db("list");
        const collection = database.collection("elements");

        const result = await collection.insertOne(el);
        console.log(`A document was inserted with the _id: ${result.insertedId}`);
    } finally {
        await client.close();
    }
}

async function deleteElement(el) {
    try {
        await client.connect();
        const database = client.db("list");
        const collection = database.collection("elements");

        const result = await collection.deleteOne(el);
        if (result.deletedCount === 1) {
            console.log("Successfully deleted one document.");
        } else {
            console.log("No documents matched the query. Deleted 0 documents.");
        }
    } finally {
        await client.close();
    }
}

async function getAll() {
    try {
        await client.connect();
        const database = client.db("list");
        const collection = database.collection("elements");

        const elements = await collection.find();
        if ((await elements.count()) === 0) {
            console.log("No documents found!");
        }
        let ans = [];
        await elements.forEach(x => ans.push(x));
        return ans;
    } finally {
        await client.close();
    }
}

async function updateElement(el) {
    try {
        await client.connect();
        const database = client.db("list");
        const collection = database.collection("elements");

        const updateDoc = {
            $set: {
                value: el.value
            },
        };
        const result = await collection.updateOne({ id: el.id }, updateDoc);
        console.log(`${result.matchedCount} document(s) matched the filter, updated ${result.modifiedCount} document(s)`);
    } finally {
        await client.close();
    }
}
module.exports = {
    createElement,
    deleteElement,
    getAll,
    updateElement
};


