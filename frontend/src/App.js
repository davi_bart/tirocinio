import React, { useEffect, useState } from 'react';
function ListElement(props) {
    return (
        <div>
            <p style={{ display: 'inline-block' }}>{props.value}</p>
            <button onClick={() => props.add()}>+</button>
            <button onClick={() => props.substract()}>-</button>
            <button onClick={() => props.remove(props.id)}>remove</button>
        </div>
    )
}

function List(props) {
    const [elements, setElements] = useState([]);

    useEffect(() => {
        fetch("/elements")
            .then((res) => res.json())
            .then((data) => setElements(data.list))
            .catch((err) => console.log(err));
    }, []);

    function createElement() {
        let x = + new Date();
        const newElement = { id: x, value: 0 };

        fetch("/elements", {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(newElement),
        }).catch((err) => console.log(err));

        setElements(elements.concat([newElement]));
    }

    function removeElement(id) {
        fetch("/elements", {
            method: 'delete',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ id: id }),
        }).catch((err) => console.log(err));

        const index = elements.findIndex(e => e.id === id);
        setElements([...elements.slice(0, index), ...elements.slice(index + 1)]);
    }

    function add(id, val) {
        const newElement = { id: id, value: elements.find(e => e.id === id).value + val };
        fetch("/elements", {
            method: 'put',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(newElement),
        }).catch((err) => console.log(err));

        const index = elements.findIndex(i => i.id === id);
        setElements([...elements.slice(0, index), newElement, ...elements.slice(index + 1)]);
    }

    return (
        <div>
            <button onClick={createElement}>add</button>
            {
                elements.map((val) =>
                    <ListElement key={val.id} id={val.id} value={val.value} remove={removeElement}
                        add={() => add(val.id, 1)} substract={() => add(val.id, -1)} />
                )
            }
            <button onClick={() => {
                fetch("/elements")
                    .then((res) => res.json())
                    .then((data) => console.log(data.list))
                    .catch((err) => console.log(err));
            }} >stampa</button>
        </div >
    );
}

function App() {
    return (
        <div className="App">
            <List />
        </div>
    );
}

export default App;
